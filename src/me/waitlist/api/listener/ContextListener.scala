package me.waitlist.api
package listener

import com.google.inject.Guice
import com.google.inject.servlet.GuiceServletContextListener
import javax.servlet.ServletContextEvent
import me.waitlist.core.traits.Loggable
import com.google.inject.Stage
import me.waitlist.api.config.ServerModule
import me.waitlist.api.config.ServletModule

/**
 * Servlet context listener for initializing dependency injection bindings.
 *
 * @author dsumera
 */
class ContextListener extends GuiceServletContextListener with Loggable {

  override def contextInitialized(sce: ServletContextEvent) {
    var millis = System.currentTimeMillis
    // prevents breakage of rest services
    //System.setProperty("javax.xml.transform.TransformerFactory",
    //  "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl")
    super.contextInitialized(sce)

    millis = System.currentTimeMillis - millis
    log.info(s"Guice initialization: $millis ms")
  }

  override protected def getInjector = ContextListener.injector
}

/** Singleton object behaves like initialization-on-demand holder (wrt synchronization) */
object ContextListener {

  /** default injector */
  val injector = Guice.createInjector(
    // specifies production deployment
    Stage.PRODUCTION,
    // guice bindings
    new ServerModule,
    // servlet bindings (e.g., RESTful endpoints)
    new ServletModule
  )
}