package me.waitlist.api
package config

import scala.collection.JavaConversions.mapAsJavaMap

import com.googlecode.objectify.ObjectifyFilter
import com.sun.jersey.api.core.PackagesResourceConfig
import com.sun.jersey.api.core.ResourceConfig
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer

/**
 * Servlet configuration for establishing filters and servlets (e.g., RESTful endpoints).
 *
 * @author dsumera
 */
class ServletModule extends me.waitlist.core.config.ServletModule {

  override def configureServlets {

    super.configureServlets

    // configure api
    serve("/*") `with` (classOf[GuiceContainer],

      // configuration parameters
      Map(
        // where to find root resources
        PackagesResourceConfig.PROPERTY_PACKAGES -> "me.waitlist.api.rs",

        // disable WADL
        ResourceConfig.FEATURE_DISABLE_WADL -> "false", // pojo mapping - MANDATORY
        
        //JSONConfiguration.FEATURE_POJO_MAPPING -> true,
        "com.sun.jersey.api.json.POJOMappingFeature" -> "true"
        ))
  }
}