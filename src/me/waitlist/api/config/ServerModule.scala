package me.waitlist.api
package config

import com.google.inject.name.Names

/**
 * Bindings for <code>api</code> module.
 *
 * @author dsumera
 */
class ServerModule extends me.waitlist.core.config.ServerModule {

  override def configure {
    
    // invoke all standard configuratoins
    super.configure
    
    // named-parameter bindings
    bind(classOf[String]) annotatedWith Names.named("url_api") toInstance "https://api.waitlist.me"
  }
}