package me.waitlist.api
package rs

import java.text.SimpleDateFormat
import java.util.Date
import java.util.HashMap
import java.util.TimeZone
import java.util.logging.Logger
import scala.collection.mutable.ListBuffer
import javax.ws.rs.core.MediaType.{APPLICATION_JSON => json, MULTIPART_FORM_DATA => data, TEXT_HTML => html, TEXT_PLAIN => text}
import javax.inject.Inject
import javax.ws.rs.DefaultValue
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import me.waitlist.core.rs.traits.Authorizable
import me.waitlist.core.actor.StatsActor
import me.waitlist.core.rs.exception.NotAuthorizedException
import me.waitlist.core.db.Customer
import me.waitlist.core.rs.exception.CustomWebApplicationException
import me.waitlist.core.dao.PlaceDao
import me.waitlist.core.db.Request
import me.waitlist.core.rs.exception.NotFoundException
import me.waitlist.core.traits.Telephony
import me.waitlist.core.rs.exception.QueryParamException
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.MimeMessage
import javax.mail.internet.AddressException
import java.util.Properties
import javax.mail.internet.InternetAddress
import javax.mail.Message
import scala.io.Source
import javax.ws.rs.core.Context
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.ws.rs.core.UriInfo
import javax.servlet.ServletContext
import com.google.appengine.repackaged.com.google.api.client.util.IOUtils
import java.util.Scanner
import me.waitlist.core.actor.StatsActor.UpdateHourly
import me.waitlist.core.actor.MailActor
import me.waitlist.core.actor.MailActor.SendWelcome
import javax.ws.rs.POST
import javax.ws.rs.FormParam
import javax.inject.Singleton

/**
 * Resource for managing <code>Request</code> related API calls.
 *
 * @author dsumera
 */
@Path("/request")
@Singleton
class RequstResource extends Authorizable
    with Telephony {

  @Inject private[this] var placeDao: PlaceDao = _
  
  @Inject private[this] var mail: MailActor = _
  @Inject private[this] var stats: StatsActor = _

  @Inject private[this] var log: Logger = _

  @Path("create")
  @GET
  @Produces(Array(json))
  def create(
    @Context ctx: ServletContext,
    @DefaultValue("")@QueryParam("key") key: String,
    @DefaultValue("-1")@QueryParam("place") place: Long,
    @DefaultValue("")@QueryParam("name") name: String,
    @DefaultValue("")@QueryParam("phone") phone: String,
    @DefaultValue("1")@QueryParam("size") size: Int,
    @DefaultValue("")@QueryParam("notes") notes: String,
    @DefaultValue("0")@QueryParam("wait") wait: Int, // in minutes
    @DefaultValue("")@QueryParam("eta") eta: String) = ifAuthorized(place, key) {

    placeDao.find(place) match {
      case Some(p) =>
        // check ability to add party remotely against api and party request preference values
        if (!isApiAllowed(p) || p.party_request_preference == "disabled")
          throw new NotAuthorizedException(s"request access for place:$place is disabled")

        // capture errors
        val errs = ListBuffer[CustomWebApplicationException]()

        // validate name
        if (name.trim.isEmpty) errs += new QueryParamException("name is required", "name", "")

        // validate size against range and place settings (1-99)
        if (size < 1) errs += new QueryParamException("size cannot be less than 1", "size", "1")

        // validate phone against range and place settings
        if (!isNumber(phone)) errs += new QueryParamException("phone number is invalid", "phone", "")

        // validate the wait time
        if (wait > 300) errs += new QueryParamException("wait cannot exceed 300 (minutes)", "wait", "0")

        // ensure eta in future
        val arrival = getDate(eta.trim, p.timezone)
        if (arrival.isDefined && arrival.get.before(new Date))
          errs += new QueryParamException(s"invalid eta: ${arrival.get}", "eta", "")
        
        // throw errors if present
        if (!errs.isEmpty) throw errs.reduce(_ += _)
        
        // TODO find or create customer
        val customer = new Customer

        // TODO create party request using correct duration
        val req = new Request

        // TODO determine the party time out value for party request duration
        //req.

        // TODO convert request to party (in waitlist) if auto add conditions are satisfied
        //      (involves sending email and sms)
        // TODO if party request preference is auto add, then eta/wait can be persisted on party automatically

        // TODO send free email quota
        //mail ! SendFree
        
        // TODO send sms
        // taskAsync("telephony")("/send_sms")()

        // stats update; can't really do anything with this java future
        stats ! UpdateHourly (
          "place" -> s"${p.id}",
          "waitlist_add" -> "1")
        
        // TODO device notification should happen over another task queue, there is no device required
        //taskAsync("notification")("/update")()
        
      //        place.party_request_preference match {
      //          case "auto_add" => error.addError(new NotFoundException(""))
      //        }

      case None => throw new NotFoundException(s"no place:$place exists")
    }

    new HashMap[String, String]()
  }

  @Path("create")
  @POST
  @Produces(Array(json))
  def createPost(
    @Context ctx: ServletContext,
    @DefaultValue("")@FormParam("key") key: String,
    @DefaultValue("-1")@FormParam("place") place: Long,
    @DefaultValue("")@FormParam("name") name: String,
    @DefaultValue("")@FormParam("phone") phone: String,
    @DefaultValue("1")@FormParam("size") size: Int,
    @DefaultValue("")@FormParam("notes") notes: String,
    @DefaultValue("0")@FormParam("wait") wait: Int, // in minutes
    @DefaultValue("")@FormParam("eta") eta: String) = create(ctx, key, place, name, phone, size, notes, wait, eta)
  
  private def getDate(eta: String, tz: String) = eta.isEmpty match {
    case true => None
    case false => try { Option(new Date(eta.toLong)) } catch {
      // parse date format
      case _: Throwable =>
        val fmt = new SimpleDateFormat("M/d/y H:m")
        fmt.setTimeZone(TimeZone.getTimeZone(tz))
        try { Option(fmt.parse(eta)) } catch {
          case _: Throwable => None
        }
    }
  }

  /*
   * name = Name of the diner - String (like brian, david).
phone = Phone number of diner - 3453453453
size = It will accept number between 1-99 for the party size
notes = It will accept string
estimated_wait = This is the estimated wait time, and will accept an integer for the time. The value needs to be less than 300
estimated_arrival_time = This is used for setting a reservation time (instead of the estimated_wait). It needs to be a Unix timestamp
   */
}