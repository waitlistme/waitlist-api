package me.waitlist.api
package rs

import javax.ws.rs.core.MediaType.{ APPLICATION_JSON => json, MULTIPART_FORM_DATA => data, TEXT_HTML => html, TEXT_PLAIN => text }
import java.util.logging.Logger
import javax.inject.Named
import scala.util.Random
import scala.collection.JavaConversions._
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils
import me.waitlist.core.rs.traits.Authorizable
import me.waitlist.core.dao.PlaceDao
import me.waitlist.core.db.User
import javax.ws.rs.Produces
import javax.ws.rs.PathParam
import me.waitlist.core.dao.PartyDao
import javax.ws.rs.core.UriBuilder
import javax.ws.rs.QueryParam
import me.waitlist.core.rs.exception.NotAuthorizedException
import javax.ws.rs.GET
import javax.ws.rs.POST
import me.waitlist.core.db.Party
import javax.ws.rs.Path
import javax.inject.Inject
import javax.ws.rs.DefaultValue
import javax.ws.rs.core.Response
import me.waitlist.core.db.Place
import me.waitlist.core.dao.crit.EQCriterion
import me.waitlist.core.dao.crit.SortCriterion
import com.googlecode.objectify.Key
import javax.ws.rs.FormParam
import javax.inject.Singleton

/**
 * Management of waitlist data.
 *
 * @author dsumera
 */
@Path("/list")
@Singleton
class ListResource extends Authorizable {

  @Inject private[this] var partyDao: PartyDao = _
  @Inject private[this] var placeDao: PlaceDao = _

  // api url
  @Inject @Named("url_api") private[this] var url: String = _

  @Path("{place: [0-9]*}")
  @GET
  @Produces(Array(json))
  def list(
    @PathParam("place") place: Long,
    @DefaultValue("")@QueryParam("key") key: String) = ifAuthorized(place, key) {
    Response.created(UriBuilder.fromUri(s"$url/list").build())
      .entity(Array(parties(place): _*))
      .build
  }

  /**
   * Supported POST method of list.
   */
  @Path("{place: [0-9]*}")
  @POST
  @Produces(Array(json))
  def listPost(
    @PathParam("place") place: Long,
    @DefaultValue("")@FormParam("key") key: String) = list(place, key)

  private def parties(place: Long) = {
    partyDao.find(
      EQCriterion("place", Key.create(classOf[Place], place)),
      EQCriterion("status", "waiting"),
      SortCriterion("-added_date"))( /* no loads */ )._1 match {
        // no waiting parties
        case Nil => Nil

        // validate api call and transform data
        case parties @ party :: _ =>
          // retrieve place
          val place = party.place.get

          // ensure place is authorized for api access
          if (isApiAllowed(place)) {
            // serialize remaining parties
            val format = place.customer_name_format
            parties map { party =>
              // convert each place to a jsonable map
              Map(
                //"place" -> party.place, // with Place load class this would load the whole model
                "id" -> party.id,
                "name" -> formatName(party, format),
                "estimated_wait" -> party.estimated_wait,
                "added_date" -> party.added_date,
                "estimated_arrival_time" -> party.estimated_arrival_time,
                "size" -> party.size): JavaMap
            }
          } else throw new NotAuthorizedException(s"place:${place.id} not authorized for api access")
      }
  }

  /**
   * Formatting for <code>Party</code> name.
   */
  private def formatName(party: Party, format: Int): String = Option(party.name) match {
    // place has no provided name
    case None => "?"

    // use preferred name format
    case Some(name) => format match {
      // just initials
      case 0 => name split (" ") map { _.charAt(0).toUpper } mkString

      // first name only
      case 1 => name split(" ") head

      // last name only
      case 2 => name split(" ") last

      // full name (3 or default)
      case _ => name
    }
  }
}