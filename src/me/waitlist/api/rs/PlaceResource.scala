package me.waitlist.api
package rs

import javax.inject.Inject
import javax.ws.rs._
import javax.ws.rs.core.MediaType.{APPLICATION_JSON => json, MULTIPART_FORM_DATA => data, TEXT_HTML => html, TEXT_PLAIN => text}
import javax.ws.rs.core.{Response, UriBuilder}
import me.waitlist.core.dao.PlaceDao
import java.util.logging.Logger
import scala.collection.JavaConversions._
import me.waitlist.core.rs.exception.NotFoundException
import javax.inject.Named
import javax.inject.Singleton

/**
 * Management of <code>Place</code>-based entities.
 *
 * @author dsumera
 */
@Path("/place")
@Singleton
class PlaceResource {

  // daos
  @Inject private[this] var placeDao: PlaceDao = _

  // url
  @Inject @Named("url_api") private[this] var url: String = _
  
  // logger
  @Inject private[this] var log: Logger = _
  
  /**
   * Retrieve <code>Place</code> information.
   *
   * @param id
   * @return
   */
  @Path("{id: [0-9]*}")
  @GET
  @Produces(Array(json))
  def read(@PathParam("id") id: String) = {
    placeDao.find(id.toLong) match {
      case Some(u) => Response.created(
        UriBuilder.fromUri("http://fourtopapp.appspot.com/api/place").build())
        .entity(u)
        .build
      case None => throw new NotFoundException("place:$place not found")
    }
  }

  /**
   * Test which rewrites fetched entity to the datastore.
   *
   * @param id
   * @return
   */
  @Path("touch/{place: [0-9]*}")
  @GET
  @Produces(Array(json))
  def touch(@PathParam("place") place: Long) = {
    placeDao.find(place) match {
      case Some(p) => Response.created(
        UriBuilder.fromUri(s"$url/place").build())
        .entity(placeDao.persist(p).values.iterator.next)
        .build
      case None => throw new NotFoundException(s"place:$place not found")
    }
  } 
  
  /**
   * Retrieve <code>Place</code> information by name.
   * @param query
   * @param limit defaults to 10
   * @return
   */
  @GET
  @Produces(Array(json))
  def placesByQuery(
    @DefaultValue("")@QueryParam("q") query: String,
    @DefaultValue("10")@QueryParam("lim") limit: Int) =
    Response.created(
      UriBuilder.fromUri(s"$url/place").build())
      // array entity must be expanded to serialize list data
      .entity(Array(placeDao.search(query, limit)._1: _*))
      .build
// {    placeDao.search(query, limit)._1 match {
//      case places@_ :: _ => Response.created(
//        UriBuilder.fromUri("https://api.waitlist.me/place").build())
//        .entity(Array(places: _*))
//        .build
//      case Nil => throw new NotFoundException
//    }
//  }
}
