package me.waitlist.api
package rs

import javax.inject.Inject
import javax.ws.rs._
import javax.ws.rs.core.MediaType.{ APPLICATION_JSON => json, MULTIPART_FORM_DATA => data, TEXT_HTML => html, TEXT_PLAIN => text }
import javax.ws.rs.core.{ Response, UriBuilder }
import java.util.logging.Logger
import javax.inject.Named
import scala.util.Random
import scala.collection.JavaConversions._
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils
import me.waitlist.core.rs.traits.Authorizable
import me.waitlist.core.dao.PlaceDao
import me.waitlist.core.db.User
import javax.inject.Singleton

/**
 * Management of <code>AuthToken</code> associated concerns.
 *
 * @author dsumera
 */
@Path("/auth")
@Singleton
class AuthResource extends Authorizable {

  // daos
  @Inject private[this] var placeDao: PlaceDao = _

  // api url
  @Inject @Named("url_api") private[this] var url: String = _

  // logger
  @Inject private[this] var log: Logger = _
  
  /**
   * Get the user login (i.e., email) and password and return api key value with
   * list of eligible <code>Place</code> entities.
   */
  @GET
  @Produces(Array(json))
  def getKey(
    @DefaultValue("")@QueryParam("email") email: String,
    @DefaultValue("")@QueryParam("pass") pass: String) = {

    // log in with credentials
    val user = login(email.trim, pass.trim)
    
    // generate the response
    Response.created(
      UriBuilder.fromUri(s"$url/auth").build())
      .entity(Map(
        // check key on user and generate if non-existent
        "key" -> Option(user.api_key).fold(resetKey(user))(identity),
        // eligible places
        "places" -> Array(places(user): _*)): JavaMap)
      .build
  }

  /**
   * Generate a new key for the given <code>User</code> credentials.
   */
  @Path("generate")
  @GET
  @Produces(Array(json))
  def generateKey(
    @DefaultValue("")@QueryParam("email") email: String,
    @DefaultValue("")@QueryParam("pass") pass: String) = {

    val user = login(email.trim, pass.trim)

    // generate the response
    Response.created(
      UriBuilder.fromUri(s"$url/auth/generate").build())
      .entity(Map(
        // check key on user and generate if non-existent
        "key" -> resetKey(user),
        // eligible places
        "places" -> Array(places(user): _*)): JavaMap)
      .build
  }

  /**
   * Supported POST method of <code>generateKey</code>.
   */
  @Path("generate")
  @POST
  @Produces(Array(json))
  def generateKeyPost(
    @DefaultValue("")@FormParam("email") email: String,
    @DefaultValue("")@FormParam("pass") pass: String) = generateKey(email, pass)

  /**
   * Supported POST method of <code>getKey</code>.
   */
  @POST
  @Produces(Array(json))
  def getKeyPost(
    @DefaultValue("")@FormParam("email") email: String,
    @DefaultValue("")@FormParam("pass") pass: String) = getKey(email, pass)

  /**
   * Get <code>Place</code>s for the provided <code>User</code>. Limit returned values
   * to those with required pricing tier.
   */
  private def places(user: User) = placeDao.find(user.place_ids: _*).values
    // limit results to places with api access
    .filter(isApiAllowed)
    // mapping place to a JSON-able data structure
    .map { place =>
      Map(
        "id" -> place.id,
        "name" -> place.name,
        // address is sequence of fields, if null replaced with empty string
        "address" ->
          Array(place.address, place.city, place.region, place.country, place.postal_code)
          .flatMap(Option(_).toList).mkString(", ")): JavaMap
    } toSeq
      
  /**
   * Persist a new key to the <code>User</code> and return the value.
   */
  private def resetKey(user: User, length: Int = 30) = {
    user.api_key = Random.alphanumeric.take(length).mkString
    userDao.persist(user)
    user.api_key
  }
}