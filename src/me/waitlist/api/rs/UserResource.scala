package me.waitlist.api
package rs

import javax.inject.Inject
import javax.ws.rs.core.MediaType.{APPLICATION_JSON => json, MULTIPART_FORM_DATA => data, TEXT_HTML => html, TEXT_PLAIN => text}
import javax.ws.rs.core.{Response, UriBuilder}
import javax.ws.rs._
import javax.inject.Named
import me.waitlist.core.dao.UserDao
import me.waitlist.core.rs.exception.NotFoundException
import me.waitlist.core.dao.crit.EQCriterion
import javax.inject.Singleton

/**
 * Management of <code>User</code>-based entities.
 *
 * @author dsumera
 */
@Path("/user")
@Singleton
class UserResource {

  // daos
  @Inject private[this] var userDao: UserDao = _

  // api url
  @Inject @Named("url_api") private[this] var url: String = _
  
  /**
   * Retrieve <code>User</code> information.
   *
   * @param id
   * @return
   */
  @Path("{user: [0-9]*}")
  @GET
  @Produces(Array(json))
  def read(@PathParam("user") user: String) = {
    userDao.find(user.toLong) match {
      case Some(u) => Response.created(
        UriBuilder.fromUri(s"$url/user").build())
        .entity(u)
        .build
      case None => throw new NotFoundException(s"user:$user not found")
    }
  }

  /**
   * Supported POST method of <code>read</code>.
   */
  @Path("{id: [0-9]*}")
  @POST
  @Produces(Array(json))
  def readPost(@PathParam("id") id: String) = read(id)

//  /**
//   * Test which rewrites fetched entity to the datastore.
//   *
//   * @param id
//   * @return
//   */
//  @Path("touch/{user: [0-9]*}")
//  @GET
//  @Produces(Array(json))
//  def touch(@PathParam("user") user: String) = {
//    userDao.find(user.toLong) match {
//      case Some(u) => Response.created(
//        UriBuilder.fromUri(s"$url/user").build())
//        .entity(userDao.persist(u).values.iterator.next)
//        .build
//      case None => throw new NotFoundException(s"user:$user not found")
//    }
//  } 
//
//  /**
//   * Retrieve <code>User</code> information by email.
//   *
//   * @param email
//   * @return
//   */
//  @Path("email/{email}")
//  @GET
//  @Produces(Array(json))
//  def userByEmail(@PathParam("email") email: String) = {
//    userDao.find(EQCriterion("email", email))()._1 match {
//      case u :: us => Response.created(
//        UriBuilder.fromUri("http://fourtopapp.appspot.com/api/user").build())
//        .entity(u)
//        .build
//      case Nil => throw new NotFoundException(s"email:$email not found")
//    }
//  }
//
//  /**
//   * Retrieve <code>User</code> information by keyword.
//   * @param query
//   * @param limit
//   * @return
//   */
//  @GET
//  @Produces(Array(json))
//  def usersByQuery(
//    @DefaultValue("")@QueryParam("q") query: String,
//    @DefaultValue("10")@QueryParam("lim") limit: Int) = {
//    userDao.search(query, limit)._1 match {
//      case users@_ :: _ => Response.created(
//        UriBuilder.fromUri("http://fourtopapp.appspot.com/api/user").build())
//        .entity(Array(users: _*))
//        .build
//      case Nil => throw new NotFoundException("no results found")
//    }
//  }
}